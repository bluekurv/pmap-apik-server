'use strict'

/*
|--------------------------------------------------------------------------
| Model and Database Factory
|--------------------------------------------------------------------------
|
| Factories let you define blueprints for your database models or tables.
| These blueprints can be used with seeds to create fake entries. Also
| factories are helpful when writing tests.
|
*/

const uuidv4 = require('uuid/v4')
const Factory = use('Factory')

/*
|--------------------------------------------------------------------------
| User Model Blueprint
|--------------------------------------------------------------------------
| Below is an example of blueprint for User Model. You can make use of
| this blueprint inside your seeds to generate dummy data.
|
*/
Factory.blueprint('App/Model/User', (fake) => {
  return {
    email: fake.email(),
    password: fake.password(),
    is_admin: false
  }
})

Factory.blueprint('App/Model/Report', (fake) => {
  return {
    submission_date: fake.date(),
    applicant: fake.word(),
    company: fake.word(),
    // lic_category: fake.pickone(['Mining', 'Plantation', 'Forestry', 'Land Title']),
    uuid: uuidv4(),
    lic_type: fake.pickone(['IUP-B (Budidaya Perkebunan)', 'IUP-P (Pengolahan Hasil Perkebunan)', 'IUP (Budidaya terintegrasi dengan pengolahan hasil perkebunan)', 'STD-B (surat tanda daftar budidaya perkebunan, max 25 Ha)', 'STB-P (surat tanda daftar pengolahan hasil perkebunan, max 25 Ha)', 'Exploration License', 'Operational License', 'IUP-HTI', 'IUP-HPH', 'IUP-RE', 'Hutan Desa', 'Hutan Kemasyarakatan', 'Hutan Tanaman Rakyat', 'Hutan Adat', 'Mini hydro power', 'Micro hydro power', 'HGU ', 'HGB', 'Hak Ulayat ']),
    phone: fake.integer(),
    status: fake.pickone(['Dalam Proses', 'Diizinkan', 'Ditolak', 'Persyaratan']),
    alamat: fake.sentence(),
    lic_stage: fake.pickone(['Izin Prinsip', 'SK Selesai', 'AMDAL', 'Lokasi']),
    user_id: fake.pickone(['1', '2'])
  }
})