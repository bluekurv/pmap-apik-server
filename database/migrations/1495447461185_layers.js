'use strict'

const Schema = use('Schema')

class LayersTableSchema extends Schema {

  up () {
    this.create('layers', (table) => {
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.text('holtikultura')
      table.text('hutan_lindung')
      table.text('kabupaten_lain')
      table.text('kaw_hut_produksi_konversi')
      table.text('kaw_hut_produksi_terbatas')
      table.text('kaw_hut_produksi')
      table.text('kawasan_bandara')
      table.text('kawasan_industri')
      table.text('kawasan_minapolitan')
      table.text('kawasan_pusat_pemerintahan')
      table.text('kawasan_rumah_potong_hewan')
      table.text('lahan_persawahan')
      table.text('pemakaman_umum')
      table.text('perkebunan')
      table.text('permukiman_pedesaan')
      table.text('permukiman_perkotaan')
      table.text('pertanian_lahan_kering')
      table.text('pltu')
      table.text('potensi_batubara')
      table.text('potensi_emas')
      table.text('ruang_terbuka_hijau')
      table.text('sempadan_sungai')
      table.text('taman_nasional')
      table.text('tubuh_air')
      table.increments()
      table.timestamps()
    })
  }

  down () {
    this.drop('layers')
  }

}

module.exports = LayersTableSchema
