'use strict'

const Schema = use('Schema')

class CreateSettingsTableTableSchema extends Schema {

  up () {
    this.create('settings', (table) => {
      table.increments()
      table.string('type')
      table.text('setting')
      table.timestamps()
    })
  }

  down () {
    this.drop('settings')
  }

}

module.exports = CreateSettingsTableTableSchema
