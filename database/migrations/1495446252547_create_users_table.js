'use strict'

const Schema = use('Schema')

class UsersTableSchema extends Schema {

  up () {
    this.create('users', table => {
      table.increments()
      table.string('uuid')
      table.string('email', 255).notNullable().unique()
      table.string('password', 255).notNullable()
      table.boolean('is_admin')
      table.string('first_name')
      table.string('last_name')
      table.string('jabatan')
      table.string('nip')
      table.string('avatar')
      table.text('setting')
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }

}

module.exports = UsersTableSchema
