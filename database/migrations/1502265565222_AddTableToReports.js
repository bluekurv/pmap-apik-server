'use strict'

const Schema = use('Schema')

class AddTableToReportsTableSchema extends Schema {

  up () {
    this.table('reports', (table) => {
      table.boolean('hasIntersect') 
      table.text('hasIntersectArea')
    })
  }

  down () {
    this.table('reports', (table) => {
      // opposite of up goes here
    })
  }

}

module.exports = AddTableToReportsTableSchema
