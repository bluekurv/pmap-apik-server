'use strict'

const Schema = use('Schema')

class EditReportsTableTableSchema extends Schema {

  up () {
    this.table('reports', (table) => {
      table.float('areaLayer')
      table.float('areaIntersect')
      table.float('areaCleared')
    })
  }

  down () {
    this.table('reports', (table) => {
      // opposite of up goes here
    })
  }

}

module.exports = EditReportsTableTableSchema
