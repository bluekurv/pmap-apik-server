'use strict'

const Schema = use('Schema')

class CreateEventsTableTableSchema extends Schema {

  up () {
    this.create('events', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.string('event')
      table.datetime('date')
      table.string('status')
      table.timestamps()
    })
  }

  down () {
    this.drop('events')
  }

}

module.exports = CreateEventsTableTableSchema
