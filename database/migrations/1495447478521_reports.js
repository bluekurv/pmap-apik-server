'use strict'

const Schema = use('Schema')

class ReportsTableSchema extends Schema {

  up () {
    this.create('reports', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.string('uuid')
      table.date('submission_date')
      table.string('applicant')
      table.string('company')
      // DOKUMEN PERSYARATAN
      table.string('doc_akte_perusahaan')
      table.string('doc_bkpm')
      table.string('doc_kepemilikan_tanah')
      table.string('doc_kesanggupan')
      table.text('doc_layer')
      table.text('doc_layer_intersect')
      table.text('doc_layer_cleared')
      table.string('doc_ktp')
      table.string('doc_npwp')
      table.string('doc_rencana_proyek')
      table.string('doc_sketsa_lokasi')
      table.string('doc_surat_permohonan')
      // DOKUMEN PERUBAHAN IZIN
      table.string('doc_pendaftaran')
      table.string('doc_pengecekan_dokumen')
      table.string('doc_pengecekan_data_spasial')
      table.string('doc_survey_lapangan')
      table.string('doc_keputusan_dinas')
      table.string('file_dokumen')
      table.string('file_surat')
      table.string('email')
      table.string('lic_stage')
      table.string('lic_type')
      table.string('phone')
      table.string('status')
      table.text('alamat')
      table.text('doc_koordinat')
      table.timestamps()
    })
  }

  down () {
    this.drop('reports')
  }

}

module.exports = ReportsTableSchema
