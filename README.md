# PMaP SIP Companion Server

### Initial Setup

1. Clone the repository
``git clone https://github.com/prasetya/pmap-sip-server.git``

2. Change directory to the cloned repository
``cd pmap-sip-server``

3. Install dependencies
``yarn``

4. Create an ``.env`` file that contains the following information, change as necessary for your development platform:
```
HOST=localhost
PORT=3333
APP_KEY={{ SOME RANDOM STRING }}
NODE_ENV=development
CACHE_VIEWS=false
SESSION_DRIVER=cookie
DB_CONNECTION=pg
DB_HOST=127.0.0.1
DB_PORT=5432
DB_USER={{ YOUR POSTGRESQL USERNAME }}
DB_PASSWORD=
DB_DATABASE=server-adonis
```

5. Create the database as described in the .env file

6. Run migration
``./ace migration:run``

7. Run database seeding
``./ace db:seed``

8. Run the server using ``yarn run serve:dev`` command

### Logging in
Run POST request to ``http://localhost:3333/login`` using the following payload:
```
{
  "email": "admin@email.com",
  "password": "jakarta"
}
```
This is the admin account, it has ``is_admin`` boolean set to ``true`` in the ``users`` table. You should receive an auth token ``token`` in the response data. Use the token in consequent requests in the header:
```
Authorization: Bearer {{token}}
```