'use strict'

const uuidv4 = require('uuid/v4')
const User = use('App/Model/User')
const Layer = use('App/Model/Layer')
const Token = use('App/Model/Token')
const Validator = use('Validator')

class AuthController {

  * register(request, response) {
    const validation = yield Validator.validateAll(request.all(), User.rules)

    if(validation.fails()) {
      response.send({ is_success: false, messages: validation.messages() })
    } else {
      const user = yield User.create({
        email: request.input('email'),
        password: request.input('password'),
        uuid: uuidv4(),
        is_admin: false
      })
      // const layer = yield User.layer().create({user_id: user.id})
      const layer = new Layer()
      // layer.user_id = user.id
      yield user.layer().save(layer)
      response.send({ is_success: true })
    }
  }

  * login (request, response) {
    const session = request.auth.authenticator('session')

    const email = request.input('email')
    const password = request.input('password')

    try {
      yield session.attempt(email, password)

      const user = yield User.findBy('email', email)
      const token = yield request.auth.generate(user, '7d')

      user.apiTokens().save(token)

      response.cookie('token', token, {
        // secure: true
      })

      if (user.is_admin == true) {
        response.status(200).send({ 
          avatar: user.avatar,
          email: user.email, 
          expiry: token.expiry, 
          firstName: user.first_name,
          is_admin: true,
          lastName: user.last_name,
          success: true, 
          token: token.token, 
          userid: user.id, 
          uuid: user.uuid
        })
      } else {
        response.status(200).send({
          avatar: user.avatar,
          email: user.email, 
          expiry: token.expiry, 
          firstName: user.first_name,
          is_admin: false,
          lastName: user.last_name,
          success: true, 
          token: token.token, 
          userid: user.id, 
          uuid: user.uuid
        })
      }

    } catch (e) {
      response.status(401).send({success: false, error: e.message})
    }
  }

  * logout(request, response) {
    const email = request.input('email')
    const token = request.input('token')
    const user = yield User.findBy('email', email)
    const user_token = yield Token.findBy('token', token)

    if(user && user_token) {
      if(user_token.user_id === user.id) {
        yield request.auth.revoke(user, token)
        response.status(200).send('Logout OK.')
      } else {
        response.status(400).send('Email and Token mismatch.')
      }
    } else {
      response.status(400).send('Email and/or Token invalid.')
    }

   }

  * checktoken (request, response) {
    const token = request.input('token')
    const user_token = yield Token.findBy('token', token)

    if(user_token) {
      if(user_token.is_revoked === true) {
        response.send({status: 'expired'})
      } else {
        response.send({status: 'active'})
      }
    } else {
      response.send({status: 'invalid'})
    }

  }

  * changepassword (request, response) {

  }

}

module.exports = AuthController
