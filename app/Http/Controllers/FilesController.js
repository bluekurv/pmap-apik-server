'use strict'

const Helpers = use('Helpers')
const Report = use('App/Model/Report')
const User = use('App/Model/User')

class FilesController {

  * upload(request, response) {
    const reportid = request.input('reportid')
    const doctype = request.input('doctype')
    const report = yield Report.findBy('id', reportid)
    const doc = request.file('doc', {
      maxSize: '10mb',
      allowedExtensions: ['jpg', 'jpeg', 'png', 'pdf', 'tif', 'xls', 'xlsx', 'doc', 'docx', 'kml', 'kmz', 'zip', 'gpx']
    })
    const fileName = `${doctype}_${new Date().getTime()}.${doc.extension()}`

    if(report) {
      try {
        yield doc.move(Helpers.storagePath('reports/' + report.id), fileName)
        if (!doc.moved()) {
          response.badRequest(doc.errors())
          return
        }
        switch(doctype) {
          case "doc_akte_perusahaan":
            report.doc_akte_perusahaan = fileName
            break
          case "doc_bkpm":
            report.doc_bkpm = fileName
            break
          case "doc_kepemilikan_tanah":
            report.doc_kepemilikan_tanah = fileName
            break
          case "doc_kesanggupan":
            report.doc_kesanggupan = fileName
            break
          case "doc_layer":
            report.doc_layer = fileName
            break
          case "doc_ktp":
            report.doc_ktp = fileName
            break
          case "doc_npwp":
            report.doc_npwp = fileName
            break
          case "doc_rencana_proyek":
            report.doc_rencana_proyek = fileName
            break
          case "doc_sketsa_lokasi":
            report.doc_sketsa_lokasi = fileName
            break
          case "doc_surat_permohonan":
            report.doc_surat_permohonan = fileName
            break
          case "doc_pendaftaran":
            report.doc_pendaftaran = fileName
            break
          case "doc_pengecekan_dokumen":
            report.doc_pengecekan_dokumen = fileName
            break
          case "doc_pengecekan_data_spasial":
            report.doc_pengecekan_data_spasial = fileName
            break
          case "doc_survey_lapangan":
            report.doc_survey_lapangan = fileName
            break
          case "doc_keputusan_dinas":
            report.doc_keputusan_dinas = fileName
            break
        }
        yield report.save()
        response.status(200).send({fileName: fileName})
      } catch (e) {
        response.send(e)
      }
    } else {
      response.send('Report not found.')
    }
  }

  * download(request, response) {
    const reportid = request.param('reportid')
    const filename = request.param('filename')

    try {
      response.status(200).attachment(Helpers.storagePath('reports/' + reportid + '/' + filename, filename))
    } catch(e) {
      response.send(e)
    }
  }
}

module.exports = FilesController
