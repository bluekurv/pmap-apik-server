'use strict'

const Event = use('App/Model/Event')

class EventsController {

  * index(request, response) {
    const events = yield Event.all()
    response.status(200).send(events)
  }

  * create(request, response) {
    const userid = request.input('userid')
    const eventMessage = request.input('event')
    const status = request.input('status')

    const event = new Event()
    event.user_id = userid
    event.event = eventMessage
    event.status = status
    event.date = new Date()

    try {
      yield event.save()
      response.status(200).send(event)
    } catch (e) {
      response.send(e)
    }
  }

}

module.exports = EventsController
