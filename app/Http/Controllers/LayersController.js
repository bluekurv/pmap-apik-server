'use strict'

const Helpers = use('Helpers')
const Layer = use('App/Model/Layer')
const User = use('App/Model/User')
const uuidv4 = require('uuid/v4')

class LayersController {

  * store(request, response) {
    const userid = request.input('userid')
    const user = yield User.findBy('id', userid)

    if(user) {
      try {
        yield user.layer().create({ uuid: uuidv4() })
        response.status(200).send('ok')
      } catch (e) {
        response.send(e)
      }
    } else {
      response.status(400).send('Invalid user.')
    }
  }

  * show(request, response) {
    const id = request.param('id')
    const layer = yield Layer.findBy('id', id)
    if (layer) {
      response.status(200).send(layer)
    } else {
      response.status(400).send('Layer not found.')
    }
  }

  * uploadFile(request, response) {
    const userid = request.param('id')
    const user = yield User.findBy('id', userid)
    const layer = yield Layer.findBy('user_id', user.id)
    const doctype = request.input('doctype')
    const doc = request.file('doc', {
      maxSize: '10mb',
      allowedExtensions: ['zip']
    })
    const fileName = `${doctype}_${new Date().getTime()}.${doc.extension()}`

    if(layer) {
      try {
        yield doc.move(Helpers.publicPath('files/' + user.uuid + '/layers'), fileName)
        if (!doc.moved()) {
          response.badRequest({error: doc.errors()})
          return
        }
        switch(doctype) {
          case "holtikultura":
            layer.holtikultura = fileName
            break
          case "hutan_lindung":
            layer.hutan_lindung = fileName
            break
          case "kabupaten_lain":
            layer.kabupaten_lain = fileName
            break
          case "kaw_hut_produksi":
            layer.kaw_hut_produksi = fileName
            break
          case "kaw_hut_produksi_konversi":
            layer.kaw_hut_produksi_konversi = fileName
            break
          case "kaw_hut_produksi_terbatas":
            layer.kaw_hut_produksi_terbatas = fileName
            break
          case "kawasan_bandara":
            layer.kawasan_bandara = fileName
            break
          case "kawasan_industri":
            layer.kawasan_industri = fileName
            break
          case "kawasan_minapolitan":
            layer.kawasan_minapolitan = fileName
            break
          case "kawasan_pusat_pemerintahan":
            layer.kawasan_pusat_pemerintahan = fileName
            break
          case "kawasan_rumah_potong_hewan":
            layer.kawasan_rumah_potong_hewan = fileName
            break
          case "lahan_persawahan":
            layer.lahan_persawahan = fileName
            break
          case "pemakaman_umum":
            layer.pemakaman_umum = fileName
            break
          case "perkebunan":
            layer.perkebunan = fileName
            break
          case "permukiman_pedesaan":
            layer.permukiman_pedesaan = fileName
            break
          case "permukiman_perkotaan":
            layer.permukiman_perkotaan = fileName
            break
          case "pertanian_lahan_kering":
            layer.pertanian_lahan_kering = fileName
            break
          case "pltu":
            layer.pltu = fileName
            break
          case "potensi_batubara":
            layer.potensi_batubara = fileName
            break
          case "potensi_emas":
            layer.potensi_emas = fileName
            break
          case "ruang_terbuka_hijau":
            layer.ruang_terbuka_hijau = fileName
            break
          case "sempadan_sungai":
            layer.sempadan_sungai = fileName
            break
          case "taman_nasional":
            layer.taman_nasional = fileName
            break
          case "tubuh_air":
            layer.tubuh_air = fileName
            break
        }
        yield layer.save()
        response.status(200).send({fileName: fileName})
      } catch (e) {
        response.send(e)
      }
    } else {
      response.send('Layers entry for this user is not found in the database.')
    }
  }

  * downloadFile(request, response) {
    const userid = request.param('id')
    const filename = request.param('filename')
    const user = yield User.findBy('id', userid)
    // response.send({filename: filename})
    response.download(Helpers.publicPath('files/' + user.uuid + '/layers'), filename)
  }

}

module.exports = LayersController
