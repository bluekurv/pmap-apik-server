'use strict'

const uuidv4 = require('uuid/v4')

const Layer = use('App/Model/Layer')
const Report = use('App/Model/Report')
const Token = use('App/Model/Token')
const User = use('App/Model/User')
const Validator = use('Validator')
const Helpers = use('Helpers')

const Hash = use('Hash')

class UsersController {

  * index(request, response) {
    const users = yield User.all()
    response.send(users)
  }

  * create(request, response) {
    //
  }

  * store(request, response) {
    //
  }

  * show(request, response) {
    const id = request.param('id')
    const user = yield User.findBy('id', id)

    if(user) {
      response.send({
        email: user.email,
        first_name: user.first_name,
        last_name: user.last_name,
        jabatan: user.jabatan,
        nip: user.nip,
        avatar: user.avatar
      })
    } else {
      response.send('No data.')
    }
  }

  * edit(request, response) {
    //
  }

  * update(request, response) {
    const id = request.param('id')
    const user = yield User.findBy('id', id)
    const password = yield Hash.make(request.input('password'))
    // const hash = Hash.make(password)

    if(user) {
      try {
        user.password = password
        yield user.save()
        response.status(200).send('ok')
      } catch (e) {
        response.send(e)
      }
    } else {
      response.send('does not match')
    }
  }

  * updateProfile(request, response) {
    const id = request.param('id')
    const user = yield User.findBy('id', id)
    const first_name = request.input('first_name')
    const last_name = request.input('last_name')
    const jabatan = request.input('jabatan')
    const nip = request.input('nip')

    if(user) {
      try {
        if (first_name) { user.first_name = first_name }
        if (last_name) { user.last_name = last_name }
        if (jabatan) { user.jabatan = jabatan }
        if (nip) { user.nip = nip }
        yield user.save()
        response.status(200).send('ok')
      } catch (e) {
        response.send(e)
      }
    } else {
      response.send('does not match')
    }
  }

  * destroy(request, response) {
    const id = request.param('id')
    const user = yield User.findBy('id', id)

    if(user) {
      const layers = yield Layer.query().where('user_id', user.id).fetch()
      const reports = yield Report.query().where('user_id', user.id).fetch()
      const tokens = yield Token.query().where('user_id', user.id).fetch()
      try {
        yield user.apiTokens().delete()
        // yield user.report().delete()
        reports.user_id = 1
        yield reports.save()
        yield user.layer().delete()
        yield user.delete()
        response.status(200).send('ok')
      } catch (e) {
        response.send(e)
      }
    } else {
      response.send('user not found.')
    }

    // try {
    //   const user = yield User.findBy('id', id)
    //   yield user.delete()
    //   response.status(200).send({ user: user, deleted: true })
    // } catch (e) {
    //   response.send(e)
    // }
  }

  * getUsersCount(request, response) {
    const users = yield User.all()
    response.send((Object.keys(users).length) - 1)
  }

  * getUserSetting(request, response) {
    const userid = request.param('id')
    const user = yield User.findBy('id', userid)

    response.status(200).send(user.setting)
  }

  * updateUserSetting(request, response) {
    const userid = request.param('id')
    const newsetting = request.input('setting')
    const user = yield User.findBy('id', userid)

    // response.status(200).send(setting)

    if(user) {
      try {
        user.setting = newsetting
        yield user.save()
        response.status(200).send('ok')
      } catch (e) {
        response.send(e)
      }
    } else {
      response.send('user not found.')
    }
  }

  * updateAvatar(request, response) {
    const id = request.param('id')
    const user = yield User.findBy('id', id)
    const doc = request.file('avatar', {
      maxSize: '10mb',
      allowedExtensions: ['jpg', 'jpeg', 'png']
    })
    const fileName = `avatar-${uuidv4()}.${doc.extension()}`

    if(user) {
      try {
        yield doc.move(Helpers.publicPath('files/' + user.uuid + '/avatars/'), fileName)
        if (!doc.moved()) {
          response.badRequest({error: doc.errors()})
          return
        }
        user.avatar = fileName
        yield user.save()
        response.status(200).send({fileName: fileName})
      } catch (e) {
        response.send(e)
      }
    } else {
      response.send('User not found.')
    }
  }

  * toggleAdminStatus(request, response) {
    const id = request.input('id')
    const user = yield User.findBy('id', id)

    try {
      user.is_admin = !user.is_admin
      yield user.save()
      response.send(user)
    } catch (e) {
      response.send(e)
    }
  }

  * getuuid(request, response) {
    const userid = request.param('id')
    const user = yield User.findBy('id', userid)
    
    if(user) {
      response.status(200).send({uuid: user.uuid})
    } else {
      response.status(400).send('User not found.')
    }
  }

}

module.exports = UsersController
