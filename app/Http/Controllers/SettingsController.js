'use strict'

const Setting = use('App/Model/Setting')

class SettingsController {

  * get(request, response) {
    const setting = yield Setting.findBy('id', 1)
    response.status(200).send(setting)
  }

  * patch(request, response) {
    const newsetting = request.input('setting')
    const setting = yield Setting.findBy('id', 1)

    try {
      setting.setting = newsetting
      yield setting.save()
      response.status(200).send('ok')
    } catch (e) {
      response.send(e)
    }
  }

}

module.exports = SettingsController
