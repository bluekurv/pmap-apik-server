'use strict'

// ARRAY file_dokumen
// 0 = doc surat permohonan
// 1 = doc akte perusahaan
// 2 = doc npwp
// 3 = doc ktp
// 4 = doc rencana proyek
// 5 = doc sketsa lokasi
// 6 = doc kepemilikan tanah
// 7 = doc bkpm
// 8 = doc kesanggupan
// 9 = doc layer
// ARRAY file_surat
// 10 = surat pendaftaran
// 11 = surat pengecekan dokumen
// 12 = surat pengecekan data spasial
// 13 = surat survey lokasi
// 14 = surat keputusan dinas

const uuidv4 = require('uuid/v4')

const Database = use('Database')
const Helpers = use('Helpers')
const storagePath = Helpers.storagePath()
const User = use('App/Model/User')
const Report = use('App/Model/Report')

const Validator = use('Validator')

class ReportsController {

  * index(request, response) {
    const user = yield User.findBy('email', request.authUser.email)

    if(user) {
      if(user.is_admin === true) {
        // const reports = yield Report.all()
        const reports = yield Database
          .table('users')
          .innerJoin('reports', 'users.id', 'reports.user_id')
        response.status(200).send(reports)
      } else {
        const reports = yield Report.query().where('user_id', user.id)
        // const users = yield User.query().where('id', user.id)
        // response.status(200).send({reports: reports, users: users})
        response.status(200).send(reports)
      }
    } else {
      response.status(401).send('You are not authenticated.')
    }

  }

  * getReportsCount(request, response) {
    const user = yield User.findBy('email', request.authUser.email)

    if(user) {
      if(user.is_admin === true) {
        // const all = yield Report.query().where('status')
        const all = yield Report.ids()
        const proses = yield Report.query().where('status', 'Dalam Proses')
        const diizinkan = yield Report.query().where('status', 'Diizinkan')
        const ditolak = yield Report.query().where('status', 'Ditolak')
        const persyaratan = yield Report.query().where('status', 'Persyaratan')
        response.status(200).send({
          all: all.length, 
          proses: proses.length,
          diizinkan: diizinkan.length,
          ditolak: ditolak.length,
          persyaratan: persyaratan.length
        })
      } else {
        const all = yield Report.query().where('user_id', user.id).ids()
        const proses = yield Report.query().where('user_id', user.id).where('status', 'Dalam Proses')
        const diizinkan = yield Report.query().where('user_id', user.id).where('status', 'Diizinkan')
        const ditolak = yield Report.query().where('user_id', user.id).where('status', 'Ditolak')
        const persyaratan = yield Report.query().where('user_id', user.id).where('status', 'Persyaratan')
        response.status(200).send({
          all: all.length, 
          proses: proses.length,
          diizinkan: diizinkan.length,
          ditolak: ditolak.length,
          persyaratan: persyaratan.length
        })
      }
    } else {
      response.status(401).send('You are not authenticated.')
    }
  }

  * create(request, response) {
    //
  }

  * store(request, response) {
    const userid = request.input('userid')

    const report = new Report()
    report.applicant = request.input('applicant')
    report.alamat = request.input('alamat')
    report.company = request.input('company')
    report.phone = request.input('phone')
    report.email = request.input('email')
    report.doc_layer = request.input('doc_layer')
    report.doc_layer_intersect = request.input('doc_layer_intersect')
    report.doc_layer_cleared = request.input('doc_layer_cleared')
    // report.lic_category = request.input('lic_category')
    report.lic_type = request.input('lic_type')
    // INTERSECT MATRIX
    report.hasIntersect = request.input('hasIntersect')
    report.hasIntersectWith = request.input('hasIntersectWith')
    report.hasIntersectArea = request.input('hasIntersectArea')
    report.user_id = userid
    report.uuid = uuidv4()
    report.submission_date = new Date()
    report.lic_stage = "Pendaftaran"
    report.status = "Dalam Proses"
    // AERA CALCULATION
    report.areaLayer = request.input('areaLayer')
    report.areaIntersect = request.input('areaIntersect')
    report.areaCleared = request.input('areaCleared')

    try {
      yield report.save()
      response.status(200).send({id: report.id})
    } catch (e) {
      response.status(400).send(e)
    }
  }

  * uploadFile(request, response) {
    const id = request.param('id')
    const report = yield Report.findBy('id', id)
    const user = yield User.findBy('id', report.user_id)
    const doctype = request.input('doctype')
    const doc = request.file('doc', {
      maxSize: '10mb',
      allowedExtensions: ['jpg', 'jpeg', 'png', 'pdf', 'tif', 'xls', 'xlsx', 'doc', 'docx', 'kml', 'kmz', 'zip', 'gpx']
    })
    const fileName = `${doctype}_${new Date().getTime()}.${doc.extension()}`

    if(report) {
      try {
        yield doc.move(Helpers.storagePath('reports/' + report.id), fileName)
        if (!doc.moved()) {
          response.badRequest(doc.errors())
          return
        }
        switch(doctype) {
          case "doc_akte_perusahaan":
            report.doc_akte_perusahaan = fileName
            break
          case "doc_bkpm":
            report.doc_bkpm = fileName
            break
          case "doc_kepemilikan_tanah":
            report.doc_kepemilikan_tanah = fileName
            break
          case "doc_kesanggupan":
            report.doc_kesanggupan = fileName
            break
          case "doc_layer":
            report.doc_layer = fileName
            break
          case "doc_ktp":
            report.doc_ktp = fileName
            break
          case "doc_npwp":
            report.doc_npwp = fileName
            break
          case "doc_rencana_proyek":
            report.doc_rencana_proyek = fileName
            break
          case "doc_sketsa_lokasi":
            report.doc_sketsa_lokasi = fileName
            break
          case "doc_surat_permohonan":
            report.doc_surat_permohonan = fileName
            break
          case "doc_pendaftaran":
            report.doc_pendaftaran = fileName
            break
          case "doc_pengecekan_dokumen":
            report.doc_pengecekan_dokumen = fileName
            break
          case "doc_pengecekan_data_spasial":
            report.doc_pengecekan_data_spasial = fileName
            break
          case "doc_survey_lapangan":
            report.doc_survey_lapangan = fileName
            break
          case "doc_keputusan_dinas":
            report.doc_keputusan_dinas = fileName
            break
        }
        yield report.save()
        response.status(200).send({fileName: fileName})
      } catch (e) {
        response.send(e)
      }
    } else {
      response.send('Report not found.')
    }
  }

  * downloadFile(request, response) {
    const id = request.param('id')
    const doctype = request.param('doctype')
    const report = yield Report.findBy('id', id)

    if(report) {
      try {
        switch(doctype) {
          case "doc_akte_perusahaan":
            response.download(Helpers.storagePath(`reports/' + ${report.doc_akte_perusahaan}`))
            break
          case "doc_bkpm":
            response.download(Helpers.storagePath(`reports/' + ${report.doc_bkpm}`))
            break
          case "doc_kepemilikan_tanah":
            response.download(Helpers.storagePath(`reports/' + ${report.doc_kepemilikan_tanah}`))
            break
          case "doc_kesanggupan":
            response.download(Helpers.storagePath(`reports/' + ${report.doc_kesanggupan}`))
            break
          case "doc_layer":
            response.download(Helpers.storagePath(`reports/' + ${report.doc_layer}`))
            break
          case "doc_ktp":
            response.download(Helpers.storagePath(`reports/' + ${report.doc_ktp}`))
            break
          case "doc_npwp":
            response.download(Helpers.storagePath(`reports/' + ${report.doc_npwp}`))
            break
          case "doc_rencana_proyek":
            response.download(Helpers.storagePath(`reports/' + ${report.doc_rencana_proyek}`))
            break
          case "doc_sketsa_lokasi":
            response.download(Helpers.storagePath(`reports/' + ${report.doc_sketsa_lokasi}`))
            break
          case "doc_surat_permohonan":
            response.download(Helpers.storagePath(`reports/' + ${report.id}/${report.doc_surat_permohonan}`))
            break
        }
      } catch (e) {
        response.send(e)
      }
    }
  }

  * show(request, response) {
    const id = request.param('id')
    const report = yield Report.findBy('id', id)

    if(report) {
      response.status(200).send(report)
    } else {
      response.send('report not found.')
    }
  }

  * edit(request, response) {
    //
  }

  * update(request, response) {
    const id = request.param('id')
    const report = yield Report.findBy('id', id)
    
    report.applicant = request.input('applicant')
    report.company = request.input('company')
    report.email = request.input('email')
    // report.lic_category = request.input('lic_category')
    report.lic_type = request.input('lic_type')
    report.phone = request.input('phone')
    report.alamat  = request.input('alamat')

    if(report) {
      try {
        yield report.save()
        response.status(200).send({stage: lic_stage})
      } catch (e) {
        response.send(e)
      }
    }
  }

  * updateStage(request, response) {
    const id = request.param('id')
    const report = yield Report.findBy('id', id)
    const lic_stage = request.input('lic_stage')
    const status = request.input('status')

    if(report) {
      try {
        report.lic_stage = lic_stage
        report.status = status
        yield report.save()
        response.status(200).send({stage: lic_stage})
      } catch (e) {
        response.send(e)
      }
    }
  }

  * destroy(request, response) {
    const id = request.param('id')
    const report = yield Report.findBy('id', id)

    if(report) {
      try {
        yield report.delete()
        response.status(200).send('ok')
      } catch (e) {
        response.send(e)
      }
    } else {
      response.send('report not found.')
    }
  }

}

module.exports = ReportsController
