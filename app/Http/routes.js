'use strict'

/*
|--------------------------------------------------------------------------
| Router
|--------------------------------------------------------------------------
|
| AdonisJs Router helps you in defining urls and their actions. It supports
| all major HTTP conventions to keep your routes file descriptive and
| clean.
|
| @example
| Route.get('/user', 'UserController.index')
| Route.post('/user', 'UserController.store')
| Route.resource('user', 'UserController')
*/

const Route = use('Route')

Route.on('/').render('welcome')

// RESOURCEFUL
Route.group('', function() {
  Route.resource('layers', 'LayersController')
  Route.resource('reports', 'ReportsController')
  Route.resource('users', 'UsersController')
}).middleware('auth')

// AUTH
Route.group('', function() {
  Route.post('logout', 'AuthController.logout')
  Route.post('register', 'AuthController.register')
  // PROFILES
  Route.patch('users/:id/updateprofile', 'UsersController.updateProfile')
  Route.post('users/:id/avatar', 'UsersController.updateAvatar')
  // SETTINGS
  Route.get('settings', 'SettingsController.get')
  Route.patch('settings', 'SettingsController.patch')
  //REPORTS
  Route.patch('reports/:id/updatestage', 'ReportsController.updateStage')
  // REPORT DOCUMENTS
  Route.post('/reports/:id/file', 'ReportsController.uploadFile')
  // LAYER DOCUMENTS
  Route.post('/users/:id/layer', 'LayersController.uploadFile')
  Route.get('/users/:id/layer/:filename', 'LayersController.downloadFile')
  // ADMIN STATUS TOGGLE
  Route.post('users/toggleadminstatus', 'UsersController.toggleAdminStatus')
  // GET USER UUID
  Route.get('users/:id/getuuid', 'UsersController.getuuid')
  // ADD EVENT LOG
  Route.post('events', 'EventsController.create')
  Route.get('events', 'EventsController.index')
  // GET REPORTS COUNT
  Route.get('reportscount', 'ReportsController.getReportsCount')
}).middleware('auth')

// FILES
  Route.post('/files', 'FilesController.upload')
  Route.get('/files/:reportid/:filename', 'FilesController.download')

// NON AUTH
Route.post('checktoken', 'AuthController.checktoken')
Route.post('login', 'AuthController.login')