'use strict'

const Hash = use('Hash')
const Lucid = use('Lucid')
const Validator = use('Validator')

class User extends Lucid {

  static boot () {
    super.boot()
    this.addHook('beforeCreate', 'User.encryptPassword')
  }

  static get rules() {
    return {
      email: 'required|email|unique:users',
      password: 'required|min:6'
    }
  }

  static get hidden () {
    return ['password']
  }

  static get messages() {
    return {
      'email.required': 'Email required',
      'password.required': 'Password required'
    }
  }

  apiTokens() {
    return this.hasMany('App/Model/Token')
  }

  layer() {
    return this.hasOne('App/Model/Layer')
  }

  report() {
    return this.hasMany('App/Model/Report')
  }

  event() {
    return this.hasMany('App/Model/Event')
  }

  // AUTH
  * login (request, response) {
    // const email = request.input('email')
    // const password = request.input('password')
    const isLoggedIn = yield request.auth.check()
  }

}

module.exports = User
