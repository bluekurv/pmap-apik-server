'use strict'

const Lucid = use('Lucid')

class Report extends Lucid {

  user () {
    return this.belongsTo('App/Model/User')
  }

  static get rules() {
    return {
    }
  }

  static get messages() {
    return {
    }
  }

}

module.exports = Report
